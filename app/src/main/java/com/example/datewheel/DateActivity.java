package com.example.datewheel;

import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

public class DateActivity extends AppCompatActivity {

    List<Fragment> listFragment = new ArrayList<>();
    List<String> listTitles = new ArrayList<>();

    TimeDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        Button bt = findViewById(R.id.bt_open);
        bt.setOnClickListener((v) -> {

            dialog = new TimeDialog(DateActivity.this);
            dialog.show();

        });
    }
}