package com.example.datewheel;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> listFragments;
    private List<String> listTitle;

    public ViewPagerAdapter(FragmentManager fragmentManager, List<Fragment> listFragments,
                            List<String> listTitle) {
        super(fragmentManager);
        this.listFragments = listFragments;
        this.listTitle = listTitle;
    }

    @Override
    public Fragment getItem(int position) {
        return listFragments.get(position);
    }

    @Override
    public int getCount() {
        return listFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listTitle.get(position);
    }
}