package com.example.datewheel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.TypedValue;


import java.util.ArrayList;
import java.util.List;
//
//public class MainActivity extends AppCompatActivity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//    }
//}

public class MainActivity extends AppCompatActivity /*implements ViewPager.OnPageChangeListener*/ {

    List<Fragment> listFragment = new ArrayList<>();
    List<String> listTitles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listFragment.add(new FirstFragment());
        listFragment.add(new SecondFragment());
        listFragment.add(new ThirdFragment());
        ViewPager viewPager = findViewById(R.id.viewPager);

//        viewPager.addOnPageChangeListener(this);//设置页面切换时的监听器(可选，用了之后要重写它的回调方法处理页面切换时候的事务)

        DepthPageTransformer2 depthPageTransformer = new DepthPageTransformer2();
        viewPager.setPageTransformer(true, depthPageTransformer);
        viewPager.setAdapter(new MyFragmentAdapter(getSupportFragmentManager(), listFragment));

        int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());
        viewPager.setPageMargin(pageMargin);

//        listTitles.add("First Page");
////        listTitles.add("Second Page");
////        listTitles.add("Third Page");
//        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),
//                listFragment, listTitles);
//        viewPager.setAdapter(viewPagerAdapter);
//
//        TabLayout tabLayout = findViewById(R.id.tabLayout);
//        tabLayout.setupWithViewPager(viewPager);
    }
}