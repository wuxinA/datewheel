package com.example.datewheel;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

public class DepthPageTransformer2 implements ViewPager.PageTransformer {
    public void transformPage(View page, float position) {
        if (position < -1 || position > 1) {
            page.setScaleX(0.9F);
            page.setScaleY(0.9F);
        } else {
            float scaleFactor = Math.max(0.9F, 1 - Math.abs(position));

            page.setScaleX(scaleFactor);
            page.setScaleY(scaleFactor);
        }
    }
}
