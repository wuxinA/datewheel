package com.example.datewheel;


import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.datewheel.adapter.MonthDayAdapter;
import com.example.datewheel.adapter.NumericWheelAdapter;
import com.wheelview.listener.OnItemSelectedListener;
import com.wheelview.view.WheelView;

import java.util.Date;

public class TimeDialog extends Dialog implements View.OnClickListener {

    private Activity activity;

    WheelView monthDayWeekWv;
    WheelView hourWv;
    WheelView minWv;
    WheelView secondWv;

    EditText queryIdEdt;

    TextView startDateTv;

    TextView currentDateTv;
    TextView currentTimeTv;

    TextView startTimeTv;

    TextView endDateTv;
    TextView endTimeTv;

    LinearLayout startLl;
    LinearLayout endLl;
    LinearLayout optionspickerLl;
    LinearLayout sureLl;

    public TimeDialog(Activity activity) {
        super(activity, R.style.ActionSheetDialogStyle);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_dividend_time2);
        setViewLocation();
        setCanceledOnTouchOutside(true);//外部点击取消
        initView();
        initData();
    }

    private void initView() {
        monthDayWeekWv = findViewById(R.id.month_day_week);
        hourWv = findViewById(R.id.hour);
        minWv = findViewById(R.id.min);
        secondWv = findViewById(R.id.second);
        queryIdEdt = findViewById(R.id.edt_query_id);

        startDateTv = findViewById(R.id.tv_start_date);
        startTimeTv = findViewById(R.id.tv_start_time);
        endDateTv = findViewById(R.id.tv_end_date);
        endTimeTv = findViewById(R.id.tv_end_time);

        startLl = findViewById(R.id.ll_start);
        endLl = findViewById(R.id.ll_end);
        optionspickerLl = findViewById(R.id.ll_optionspicker);
        sureLl = findViewById(R.id.ll_sure);

        endLl.setOnClickListener(this);
        startLl.setOnClickListener(this);
        optionspickerLl.setOnClickListener(this);
        sureLl.setOnClickListener(this);
    }

    private Date now;
    private MonthDayAdapter monthDayAdapter;
    private NumericWheelAdapter hourAdapter;
    private NumericWheelAdapter minAdapter;
    private NumericWheelAdapter secondAdapter;

    private MonthDay monthDay;

    /**
     * 初始化设置转轮的值
     */
    private void initData() {
        now = new Date();// 初始化为当前年 月 日 时  分和秒为0
        monthDayWeekWv.isSpecial(true);
        monthDayAdapter = new MonthDayAdapter(now, 29);
        monthDayWeekWv.setCyclic(false);
        monthDayWeekWv.setAdapter(monthDayAdapter);
        monthDayWeekWv.setCurrentItem(0);// 初始化为0

        currentDateTv = startDateTv;
        currentTimeTv = startTimeTv;

        monthDayWeekWv.setOnItemSelectedListener(index -> {
            // 设置时间变化
            monthDay = monthDayAdapter.getItem(index);
            currentDateTv.setText(monthDay.getDateStr());
        });

        hourAdapter = new NumericWheelAdapter(0, 23);
        hourWv.setAdapter(hourAdapter);
        hourWv.setCurrentItem(now.getHours() - 1);//从0 开始

        minAdapter = new NumericWheelAdapter(0, 59);
        minWv.setAdapter(minAdapter);
        minWv.setCurrentItem(0);

        secondAdapter = new NumericWheelAdapter(0, 59);
        secondWv.setAdapter(secondAdapter);
        secondWv.setCurrentItem(0);

        OnItemSelectedListener onItemSelectedListener = index -> {
            int hourItem = hourWv.getCurrentItem();
            int minItem = minWv.getCurrentItem();
            int secondItem = secondWv.getCurrentItem();
            currentTimeTv.setText(String.format("%02d", hourAdapter.getItem(hourItem))
                    + ":" + String.format("%02d", minAdapter.getItem(minItem))
                    + ":" + String.format("%02d", secondAdapter.getItem(secondItem))
            );
        };

        hourWv.setOnItemSelectedListener(onItemSelectedListener);
        minWv.setOnItemSelectedListener(onItemSelectedListener);
        secondWv.setOnItemSelectedListener(onItemSelectedListener);

        startDateTv.setText(monthDayAdapter.getItem(0).getDateStr());
        startTimeTv.setText(String.format("%02d", hourAdapter.getItem(now.getHours() - 1))
                + ":" + String.format("%02d", minAdapter.getItem(0))
                + ":" + String.format("%02d", secondAdapter.getItem(0)));

        endDateTv.setText(monthDayAdapter.getItem(0).getDateStr());
        endTimeTv.setText(String.format("%02d", hourAdapter.getItem(now.getHours() - 1))
                + ":" + String.format("%02d", minAdapter.getItem(0))
                + ":" + String.format("%02d", secondAdapter.getItem(0)));
    }

    /**
     * 设置dialog位于屏幕底部
     */
    private void setViewLocation() {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;

        Window window = this.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.x = 0;
        lp.y = height;
        lp.width = ViewGroup.LayoutParams.MATCH_PARENT;
        lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        // 设置显示位置
        onWindowAttributesChanged(lp);
    }

    private boolean tag_layoutTop = true;

    @Override
    public void onClick(View v) {
        if (v == sureLl) {
            // 返回 dialog 的返回值， 并进行关闭
        } else if (v == startLl) {
            //进行如果ll_end 在顶部， 那么移动到下面
            if (!tag_layoutTop) {
                ObjectAnimator.ofFloat(optionspickerLl, "TranslationY", 0).setDuration(300).start();
                ObjectAnimator.ofFloat(endLl, "TranslationY", 0).setDuration(300).start();
                tag_layoutTop = true;
                currentDateTv = startDateTv;
                currentTimeTv = startTimeTv;
            }
            // 并设置初始值
        } else if (v == endLl) {
            // 进行移动
            int endLlDistance = endLl.getHeight();
            int optionspickerLlDistance = optionspickerLl.getHeight();
            if (tag_layoutTop) {
                ObjectAnimator.ofFloat(optionspickerLl, "TranslationY", endLlDistance).setDuration(300).start();
                ObjectAnimator.ofFloat(endLl, "TranslationY", -optionspickerLlDistance).setDuration(1000).start();
                tag_layoutTop = false;
                currentDateTv = endDateTv;
                currentTimeTv = endTimeTv;

            } else {
                ObjectAnimator.ofFloat(optionspickerLl, "TranslationY", 0).setDuration(1000).start();
                ObjectAnimator.ofFloat(endLl, "TranslationY", 0).setDuration(300).start();
                tag_layoutTop = true;
                currentDateTv = startDateTv;
                currentTimeTv = startTimeTv;
            }
        }
    }
}
