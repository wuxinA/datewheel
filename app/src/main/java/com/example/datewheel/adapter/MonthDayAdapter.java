package com.example.datewheel.adapter;

import android.util.SparseArray;

import com.example.datewheel.MonthDay;
import com.wheelview.adapter.WheelAdapter;

import java.util.Calendar;
import java.util.Date;


/**
 * 描述：
 */
public class MonthDayAdapter implements WheelAdapter {

    private Calendar calendar = Calendar.getInstance();

    SparseArray<MonthDay> data = new SparseArray();

    // length
    private int length;
    private Date date;

    /**
     * Constructor
     *
     * @param length the max items length
     */
    public MonthDayAdapter(Date date, int length) {
        calendar.setTime(date);
//        String additional = null;
//        if (calendar.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)
//                && calendar.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
//                && calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)
//        ) {
//            additional = "今日";
//        }
        MonthDay monthDay = new MonthDay(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH)+1,
                calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.DAY_OF_WEEK), null);
        data.put(0, monthDay);
        this.length = length;
    }


    @Override
    public MonthDay getItem(int index) {
        MonthDay value = data.get(index);
        if (value == null) {
            value = getDate(calendar, index, data);
            data.put(index, value);
        }
        return value;
    }

    @Override
    public int indexOf(Object o) {
        int i = data.indexOfValue((MonthDay) o);
        if (i < 0) {
            return 0;
        }
        return i;
    }

    private static synchronized MonthDay getDate(Calendar calendar, int x, SparseArray<MonthDay> data) {
        if (data.indexOfKey(x) < 0) {
            calendar.add(Calendar.DATE, x);
            MonthDay monthDay = new MonthDay(calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH)+1,
                    calendar.get(Calendar.DAY_OF_MONTH),
                    calendar.get(Calendar.DAY_OF_WEEK), null);
            calendar.add(Calendar.DAY_OF_MONTH, -x);
            return monthDay;
        }
        return data.get(x);
    }

    @Override
    public int getItemsCount() {
        return length;
    }

}
