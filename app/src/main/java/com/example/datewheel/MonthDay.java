package com.example.datewheel;

/**
 * 描述：
 */

public class MonthDay {

    int year;

    int month;

    int day;

    int week;

    String item;

    public MonthDay(int year, int month, int day, int week, String item) {
        this.year = year;
        this.month = month;
        this.day = day;

        if (item == null) {
            this.item = month + "月" + day + "日 " + getDayOfWeekCN(week);
        } else {
            this.item = item;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getWeek() {
        return week;
    }

    public void setWeek(int week) {
        this.week = week;
    }

    @Override
    public String toString() {
        return item;
    }

    public String  getDateStr() {
        return year + "年" + (month > 9 ? (month) : ("0" + month)) + "月" + (day > 9 ? day : "0" + day) + "日";
    }

    public static String getDayOfWeekCN(int day_of_week){
        String result = null;
        switch(day_of_week){
            case 1:
                result = "周日";
                break;
            case 2:
                result = "周一";
                break;
            case 3:
                result = "周二";
                break;
            case 4:
                result = "周三";
                break;
            case 5:
                result = "周四";
                break;
            case 6:
                result = "周五";
                break;
            case 7:
                result = "周六";
                break;
            default:
                break;
        }
        return result;
    }
}
