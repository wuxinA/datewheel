package com.wheelview.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
